from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.urls import reverse_lazy
import uuid


class Project(models.Model):

    class Meta:
        pass
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    title = models.CharField(max_length=30)
    desc = models.TextField(max_length=500)
    due_date = models.DateField()
    max_time = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now=True)
    progress = models.PositiveIntegerField(
        default=0,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(0)
        ]
    )
    authors = models.ManyToManyField(User)
    slug = models.SlugField(null=False, unique=True)

    def get_absolute_url(self):
        return reverse_lazy('projects:project-detail', args=[self.slug])

    def __str__(self):
        return self.title


class Activity(models.Model):

    class Meta:
        ordering = ['-timestamp',]
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'
    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    snip = models.CharField(max_length=80, default='Made some contributions')
    data = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.timestamp

class Task(models.Model):
    id = models.AutoField(primary_key=True)
    parent = models.ForeignKey('Task', on_delete=models.CASCADE, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    progress = models.PositiveIntegerField(
        default=0,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(0)
        ]
    )
    title = models.CharField(max_length=32, null=False)
    desc = models.TextField(null=True)
    timestamp = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


