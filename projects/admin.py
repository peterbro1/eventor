from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportMixin
from .models import Project, Activity

class ProjectModelResouce(resources.ModelResource):
    class Meta:
        model = Project

class ActivityModelResource(resources.ModelResource):
    class Meta:
        model = Activity

@admin.register(Project)
class ProjectModelAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = ProjectModelResouce

@admin.register(Activity)
class ActivityModelAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = ActivityModelResource
