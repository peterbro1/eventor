from django.urls import path, include
from . import views

app_name = 'projects'
urlpatterns = [
    path('', views.ProjectIndex.as_view()),
    path('<slug:slug>', views.ProjectView.as_view(), name='project-detail')
]
