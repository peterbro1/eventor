import django_tables2 as tables
from .models import Project


class PaddedColumn(tables.Column):
    attrs = {
        'th':
            {
                'class': 'py-lg-3 table-header'
            }
    }

class ProjectTable(tables.Table):
    id = PaddedColumn()
    title = PaddedColumn(linkify=True)
    desc = PaddedColumn()
    progress = PaddedColumn()
    authors = PaddedColumn()

    class Meta:
        model = Project
        attrs = {'class': 'table table-striped table-hover py-lg-3'}
        fields = ('title', 'desc', 'progress', 'authors')

