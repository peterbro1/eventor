from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, ListView
import django_tables2 as tables
from .models import Project
from .tables import ProjectTable
from .models import Activity

class ProjectIndex(tables.SingleTableMixin, ListView):
    table_class = ProjectTable
    template_name = 'projects/index.html'
    model = Project

class ProjectView(DetailView):
    template_name = 'projects/project-detail.html'
    model = Project
    def get_context_data(self, **kwargs):
        context = super(ProjectView, self).get_context_data(**kwargs)
        context['related_activity'] = Activity.objects.filter(project__id__exact=context['project'].id).order_by('-timestamp')
        return context



