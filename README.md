## Eventor

This project is meant to be an ERP/project management software. I originally intended to make this as an in-house replacement for dolibarr or monday.com type project software, but I chose a bad time to start it, as work began to pile up shortly afterwards.

All CSS, JS, HTML, and Python code is original. Feel free to use and/or learn from it. But keep in mind that this was more of a validation project for me to learn and get comfortable using Django. I'm sure there are lots of mistakes-- and if I ever get back to making it, I will push all changes.

